import subprocess
from pathlib import Path
from io import StringIO
from pathlib import Path

from gryml.cli import print_definition
from gryml.core import Gryml

def build_bases():
    subprocess.call(f"gryml -f ci/configs/values.gryml.yml > ci/service_core.yml", shell=True)


if __name__ == '__main__':
    build_bases()
