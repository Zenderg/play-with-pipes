FROM python:3.7-buster

ARG SERVICE

# TODO: this is only needed in some services
RUN apt-get update -y && apt-get install -y zlib1g-dev libjpeg-dev python3-pythonmagick inkscape xvfb poppler-utils libfile-mimeinfo-perl\
 qpdf libimage-exiftool-perl ufraw-batch ffmpeg

RUN apt-get update && apt-get install -y gdal-bin python-gdal libgdal-dev
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal
RUN pip3 install pygdal==2.4.0.4

COPY schemas /schemas
COPY "backend/" "/package/"
#COPY "backend/services/$SERVICE" "/package/services/$SERVICE/"

#COPY backend/common/ /package/common/
#COPY "backend/services/$SERVICE/requirements.txt" "/package/services/$SERVICE/requirements.txt"

RUN pip3 install -r "/package/services/$SERVICE/requirements.txt"

WORKDIR "/package/common"

#CMD bash -c "python3 ./seed/devbasic.py && cd /package/services/fuse/ && python3 ./service.py"
