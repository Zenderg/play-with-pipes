FROM node:10-alpine as builder

WORKDIR /usr/src/app

COPY client/package.json ./
COPY client/yarn.lock ./
COPY client/package-lock.json ./

ARG configuration=production
ARG server_url

ENV SERVER_URL=$server_url

RUN npm i

COPY client/ .

RUN npm run build


FROM nginx:1.17.9-alpine

COPY deployment/nginx/default.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /usr/src/app/dist/skriptum /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
